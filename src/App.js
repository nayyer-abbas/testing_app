import logo from './logo.svg';
import Home from './home/home.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  return (
    <Home></Home>
  );
}

export default App;
