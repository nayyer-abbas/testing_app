import reactDom from 'react-dom';
import { Container, Row, Col } from 'react-bootstrap';
import './home.css';

function Home(){
    return(
        <section>
            <Container>
                <Row>
                    <Col md="12" className="text-center">
                        <h1>This is test app</h1>
                    </Col>
                </Row>
            </Container>
        </section>
    );
}

export default Home;